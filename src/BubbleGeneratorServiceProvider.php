<?php 

namespace BubbleGenerator\BUBBLE;
use Illuminate\Support\ServiceProvider;

/**
* 
*/
class BubbleGeneratorServiceProvider extends ServiceProvider
{
	
public function boot(){
	include __DIR__ .'/routes.php';
	$this->loadViewsFrom(__DIR__.'/Views','bubblegenerator');
}

public function register(){

	$this->app['bubble'] = $this->app->share(function($app){
		return new BUBBLE;

	});

}

}